# Motivator

Application In-Progress!!!

Motivator is a simple Django application for tracking your repetitive tasks.
Using cloud.mongodb.com as Mongo Database.

Application should be run locally only (so far)!

## Installation
Install Python 3.x.y

Install django via pip (recommended way):
```bash
python -m pip install Django
```

## Usage

Clone this repo and run:

```bash
python3 manage.py runserver
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

