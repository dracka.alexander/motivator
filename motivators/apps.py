from django.apps import AppConfig


class MotivatorsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'motivators'
