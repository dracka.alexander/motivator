import pygal
from django.http import HttpResponse
from django.shortcuts import render
from datetime import date, timedelta, datetime

# Create your views here.
from soupsieve.util import string

import utils
from motivators.forms import FormTask, FormTaskEdit


def tasks_report(request: object) -> object:
    """
    Delivers main web page content.
    List of all active tasks.
    Form for creating a new task.
    @param request: request object
    @return: HttpResponse object
    """
    context = {}
    if request.method == 'POST':
        form = FormTask(request.POST)
        if form.is_valid():
            utils.set_new_task(request.POST['task_name'])

    form_task = FormTask()
    today: str = date.today().strftime("%d.%m.%Y")
    tasks: dict = utils.get_current_tasks_for_user()

    context['tasks'] = tasks
    context['today'] = today
    context['form'] = form_task
    # todo only active tasks
    for key, value in tasks.items():
        # TODO: we assume value of frequency only: daily | weekly, this should be checked
        # eg. key= cybrary
        # eg. value={'status': True, 'frequency': 'daily', 'dates': {}, 'info': ''}

        if tasks[key]['frequency'] == 'daily':
            tasks[key]['dates'] = True if today in tasks[key]['dates'] else False
        elif tasks[key]['frequency'] == 'weekly':
            date_list = [(date.today() - timedelta(days=x)).strftime("%d.%m.%Y") for x in range(7)]
            tasks[key]['dates'] = any(d for d in date_list if d in tasks[key]['dates'])
    # render() combines a given template with a given context dict and return HttpResponse object with rendered text
    return render(request, 'index.html', context)


def finish_task(request: object, task: string) -> object:
    """
    Logic for marking task as finished. It will redirect to new page.
    @param request: HttpRequest object
    @param task: name of the task, which is part of url
    @return: Simple HttpResponse
    """
    # task is name of the task as displays in list
    result = utils.mark_task_finished(task)
    if not result:
        return HttpResponse(f'Task does not exist. Not cool man.{result}')
    return render(request, 'finished.html', {'task': task})


def task_edit(request: object, task: string) -> object:
    """
    Edit form of existing Task.
    @param request: HttpRequest object
    @param task: name of the task, which is part of url
    @return: HttpResponse object
    """
    if request.method == 'POST':
        form = FormTaskEdit(request.POST)
        if form.is_valid():
            utils.update_task_detail(form.cleaned_data, task)

    # todo improve ignoring/handling the request with fake task
    task_detail = utils.get_task_details(task)
    # if task is empty (bool(task) => False)-> task does not exist in DB
    if not bool(task_detail):
        return HttpResponse(f'Task ->{task}<-does not exist. Not cool man.')
    form_task_edit = FormTaskEdit(initial={'task_name': f'{task}', 'is_task_active': task_detail['status'],
                                           'task_frequency': task_detail['frequency'], 'task_info': task_detail['info']})
    return render(request, 'edit.html', {'task': task, 'form': form_task_edit})


def task_history(request, task):

    history_dict = utils.history_task(task)
    if history_dict == {}:
        return HttpResponse(f'Task ->{task}<-does not exist. Not cool man.')
    context = {'task': task, 'history': history_dict}
    print(f'++++++++{history_dict}')
    return render(request, 'history.html', context)
