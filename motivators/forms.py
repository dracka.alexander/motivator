from django import forms


class FormTask(forms.Form):
    # each field would be mapped as an input field in HTML
    task_name = forms.CharField(max_length=30)


class FormTaskEdit(forms.Form):
    # each field would be mapped as an input field in HTML
    is_task_active = forms.BooleanField()
    task_frequency = forms.CharField(required=False, max_length=50)
    task_info = forms.CharField(required=False, max_length=500)
