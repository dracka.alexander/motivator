from django.urls import reverse, resolve
from django.test import TestCase

from motivators.views import tasks_report


class HomeTests(TestCase):
    def test_home_views_status_code(self):
        """Is the website reachable? If not it will return a status code 500: Internal Server Error"""
        url = reverse('index')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_home_url_resolves_home_view(self):
        """
        Using resolver function to match a requested URL with a list of URLs listed in the urls.py module
        Checking if requested url is returning task_report view
        """
        view = resolve('/')
        self.assertEquals(view.func, tasks_report)
