from django.urls import path

from . import views

urlpatterns = [
    # name argument is useful for Naming URL patterns
    # more here https://docs.djangoproject.com/en/4.1/topics/http/urls/#naming-url-patterns
    path('', views.tasks_report, name='index'),
    path('<str:task>/', views.finish_task, name='task-finished'),
    # In-progress urls
    path('<str:task>/edit/', views.task_edit, name='task-edit'),
    path('<str:task>/history/', views.task_history, name='task-history')

]
