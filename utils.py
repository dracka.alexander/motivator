import os
import string
from datetime import date, timedelta

import pygal
import pymongo.errors
from django.http import Http404
from pymongo import MongoClient, ReturnDocument


# this is one way of using PyMongo's MongoClient
# TODO remove?
import motivators.forms


def get_db_handle(db_name, host, port, username, password):
    client = MongoClient(host=host, port=int(port), username=username, password=password)
    db_handle = client['db_name']
    return db_handle, client


def get_mongo_client() -> pymongo.MongoClient:
    host: str = os.getenv("DB_HOST")
    credentials: str = os.getenv("DB_CREDENTIALS")
    connection_string = f'mongodb+srv://{credentials}@{host}/?retryWrites=true&w=majority'
    client = MongoClient(connection_string)
    print(f"client:{type(client)}")
    return client


# this is another option of using PyMongo's MongoClient with MongoDB connection string
def get_db_data_via_connection_string() -> dict:
    client = get_mongo_client()

    # define DB name
    db = client['motivatorDB']

    # define Collection
    collection = db['users']
    try:
        # find method returns pymongo.cursor.Cursor instance; find_one will return first document as a dictionary
        collection_details = collection.find_one({"name": "Alexander"})
    except pymongo.errors.OperationFailure:
        print('Error! Authentication Failed, do you have correct credentials?')
    finally:
        client.close()
    return collection_details


def get_current_tasks_for_user() -> dict:
    """
    Return dictionary of user tasks with their status for this day/week
    """
    data_dict = get_db_data_via_connection_string()
    return data_dict['tasks']


def set_new_task(task_name) -> None:
    """
    New task will have
    @param task_name:
    @return:
    """
    # What are requirements for name of the task?
    client = get_mongo_client()
    # define DB name
    db = client['motivatorDB']

    # define Collection
    collection = db['users']

    # if identical task exists it will overwrite -> loosing all data in dates object => so we are checking user's tasks
    user_tasks = collection.find_one({'name': 'Alexander'})
    if task_name not in user_tasks['tasks']:
        # todo verify result? does that make sense?
        collection.find_one_and_update({'name': 'Alexander'},
                                       {'$set': {f'tasks.{task_name}': {'status': True, 'frequency': 'daily',
                                                                        'dates': {}, 'info': ''}}})
    else:
        raise Http404('Task already exists')


def is_task_real(collection, task) -> bool:
    # Return true if task exists in DB under user otherwise False
    if task in collection.find_one({'name': 'Alexander'})['tasks']:
        return True
    return False


def mark_task_finished(task) -> bool:
    """
    Mark and store specific task in Mongo DB as finished for a current day. Eg dates: {'29.9.2022': True}
    @param task: name of the task. Based on url
    """
    today: str = date.today().strftime("%d.%m.%Y")

    client = get_mongo_client()
    # define DB name
    db = client['motivatorDB']

    # define Collection
    collection = db['users']

    # check if task is valid value (in other words is this task in DB?).
    if task in collection.find_one({'name': 'Alexander'})['tasks']:
        task_dates = collection.find_one({'name': 'Alexander'})['tasks'][task]['dates']
        if today not in task_dates:
            # add today's date into dictionary
            task_dates[today] = True
            # TODO: should we check result?
            result = collection.find_one_and_update({'name': 'Alexander'},
                                                    {'$set': {f'tasks.{task}.dates': task_dates}}, return_document=True)
            return True
        else:
            # task with current day is already in task's dates object
            # therefore no DB write but returning True as the task is finished already
            return True
    return False


def get_task_details(task) -> dict:
    client = get_mongo_client()
    # define DB name
    db = client['motivatorDB']

    # define Collection
    collection = db['users']
    if is_task_real(collection, task):
        task = collection.find_one({'name': 'Alexander'})['tasks'][task]
        return task
    else:
        return {}


def update_task_detail(task_details: dict, task_name: string) -> dict:
    client = get_mongo_client()
    # define DB name
    db = client['motivatorDB']

    # define Collection
    collection = db['users']

    # check if task is valid value (in other words: is this task in the DB?).
    task = {'status': task_details['is_task_active'], 'frequency': task_details['task_frequency'],
            'info': task_details['task_info']}
    if task_name in collection.find_one({'name': 'Alexander'})['tasks']:
        # todo check result?
        result = collection.find_one_and_update({'name': 'Alexander'},
                                                {'$set': {f'tasks.{task_name}.frequency': task['frequency'],
                                                          f'tasks.{task_name}.info': task['info'],
                                                          f'tasks.{task_name}.status': task['status']}})


def history_task(task) -> dict:
    """return a dictionary of dates for a given task"""
    client = get_mongo_client()
    # define DB name
    db = client['motivatorDB']

    # define Collection
    collection = db['users']

    if is_task_real(collection, task):
        task_dates_dict = collection.find_one({'name': 'Alexander'})['tasks'][task]['dates']
    else:
        return {}
    date_list = [(date.today() - timedelta(days=x)) for x in range(30)]
    date_list.reverse()

    real_history_dict = {}
    for day in date_list:
        if day.strftime("%d.%m.%Y") not in task_dates_dict:
            real_history_dict[day.strftime("%d.%m.%Y")] = False
        else:
            real_history_dict[day.strftime("%d.%m.%Y")] = True
    return real_history_dict
